var PORT = process.env.PORT || 8080;

var redis = require('redis');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var $ = require('cheerio');
var request = require('request');
var _ = require('underscore');

var config = require('./config').config;

app.use(express.bodyParser());

var oneDay = 86400000;
app.use(express.static('public', { maxAge: oneDay }));


var client = redis.createClient(config.REDIS_PORT, config.REDIS_HOST);
console.log('connected to redis: ' + config.REDIS_HOST + ':' + config.REDIS_PORT);
client.auth(config.REDIS_PASS, function() {
    console.log('redis authorised');
});
client.on("error", function (err) {
    console.error("redis error: ", err);
});

var poolPlayers = {
    "Daniel Negreanu": { url: "http://www.bluff.com/players/daniel-negreanu/tournament-results/267/" },
	"Jason Mercier": { url: "http://www.bluff.com/players/jason-mercier/tournament-results/42898/" },
	"Bertrand (Elky) Grospellier": { url: "http://www.bluff.com/players/bertrand-grospellier/tournament-results/32116/" },
	"Eugene Katchalov": { url: "http://www.bluff.com/players/eugene-katchalov/tournament-results/1343/" },
	"John Juanda": { url: "http://www.bluff.com/players/john-juanda/tournament-results/261/" },
	"Jonathan Karamalikis": { url: "http://www.bluff.com/players/jonathan-karamalikis/tournament-results/39764/" },
	"Ana Marquez": { url: "http://www.bluff.com/players/ana-marquez/tournament-results/88879/" },
	"Shawn Buchanan": { url: "http://www.bluff.com/players/shawn-buchanan/tournament-results/1393/" },
	"Matt Glantz": { url: "http://www.bluff.com/players/matt-glantz/tournament-results/616/" },
	"John Monnette": { url: "http://www.bluff.com/players/john-monnette/tournament-results/4028/" },
	"Phil Ivey": { url: "http://www.bluff.com/players/phil-ivey/tournament-results/273/" },
	"Phil Hellmuth": { url: "http://www.bluff.com/players/phil-hellmuth/tournament-results/271/" },
	"Marvin Rettenmaier": { url: "http://www.bluff.com/players/marvin-rettenmaier/tournament-results/82068/" },
	"Paul Volpe": { url: "http://www.bluff.com/players/paul-volpe/tournament-results/53967/" },
	"Maria Ho": { url: "http://www.bluff.com/players/maria-ho/tournament-results/4368/" },
	"Daniel Kelly": { url: "http://www.bluff.com/players/daniel-kelly/tournament-results/35756/" },
	"Justin Bonomo": { url: "http://www.bluff.com/players/justin-bonomo/tournament-results/1389/" },
	"Jeffrey Lisandro": { url: "http://www.bluff.com/players/jeffrey-lisandro/tournament-results/745/" },
	"Toby Lewis": { url: "http://www.bluff.com/players/toby-lewis/tournament-results/65252/" },
	"Jarred Solomon": { url: "http://www.bluff.com/players/jarred-solomon/tournament-results/52009/" },
	"Aaron Lim": { url: "http://www.bluff.com/players/aaron-lim/tournament-results/120180/" },
	"Joe Hachem": { url: "http://www.bluff.com/players/joe-hachem/tournament-results/3426/" },
	"Shaun Deeb": { url: "http://www.bluff.com/players/shaun-deeb/tournament-results/26176/" },
	"Alexander Kostritsyn": { url: "http://www.bluff.com/players/alexander-kostritsyn/tournament-results/40410/" },
	"Thayer Rasmussen": { url: "http://www.bluff.com/players/thayer-rasmussen/tournament-results/19953/" },
	"Xuan Liu": { url: "http://www.bluff.com/players/xuan-liu/tournament-results/80564/" },
	"David (Bakes) Baker": { url: "http://www.bluff.com/players/david-baker/tournament-results/1915/" },
	"Johnny Lodden": { url: "http://www.bluff.com/players/johnny-lodden/tournament-results/29360/" },
	"Sorel Mizzi": { url: "http://www.bluff.com/players/sorel-mizzi/tournament-results/29488/" },
	"Steve O'Dwyer": { url: "http://www.bluff.com/players/steve-o'dwyer/tournament-results/26145/" },
	"Victor Ramdin": { url: "http://www.bluff.com/players/victor-ramdin/tournament-results/720/" },
	"Martin Finger": { url: "http://www.bluff.com/players/martin-finger/tournament-results/117122/" },
	"Stephen Chidwick": { url: "http://www.bluff.com/players/stephen-chidwick/tournament-results/42335/" },
	"Hui (Kitty) Chen-Kuo": { url: "http://www.bluff.com/players/hui-chen-kuo/tournament-results/68620/" },
	"Gregory Ronaldson": { url: "http://www.bluff.com/players/gregory-ronaldson/tournament-results/102685/" },
	"Kosta Mamaliadis": {},
	"Takashi Ogura": { url: "http://www.bluff.com/players/Takashi-Ogura/tournament-results/62880/"}
};

app.get('/api/players', function(req, res) {
    console.log('GET /api/players');
    res.set('Content-Type', 'application/json');
    client.zrevrange('wsop-poy:2013:points', 0, 499, 'WITHSCORES', function(err, data) {
    	var players = [];
    	var rank = 1;
    	for (var i = 0; i < data.length; i += 2) {
    		players.push({ rank: rank, name: data[i], points: Number(data[i+1]).toFixed(2) });
    		rank++;
    	}
    	client.get('wsop-poy:2013:points:last-updated', function(err, data) {
    		var lastUpdated = Number(data);
    		res.send(JSON.stringify({ players: players, lastUpdated: lastUpdated }));
    	});
    });
});

app.get('/api/pool-players', function(req, res) {
    console.log('GET /api/pool-players');
    res.set('Content-Type', 'application/json');
    var clientMulti = client.multi();
    var playerNames = [];
    for (var poolPlayerName in poolPlayers) {
    	var playerPointsKey = 'player:' + poolPlayerName + ':tournament-points';
    	playerNames.push(poolPlayerName);
    	clientMulti.hgetall(playerPointsKey);
    }
    clientMulti.get('tournament-results:last-updated');
    clientMulti.exec(function(err, replies) {
    	var players = {};
    	var tournaments = {};
    	for (var i = 0; i < replies.length - 1; i++) {
    		players[playerNames[i]] = { points: 0, url: poolPlayers[playerNames[i]].url || '' };
    		var tourneyPoints = replies[i] || {};
    		for (var tourneyName in tourneyPoints) {
    			tournaments[tourneyName] = tournaments[tourneyName] || {};
    			var tourneyPointsNumber = Number(tourneyPoints[tourneyName]);
    			tournaments[tourneyName][playerNames[i]] = tourneyPointsNumber;
    			players[playerNames[i]].points += Number(tourneyPointsNumber);
    		}
    		players[playerNames[i]].points = Number(players[playerNames[i]].points.toFixed(2));
    		// console.log(playerNames[i], players[playerNames[i]].points);
    	}
    	var lastUpdated = Number(replies[replies.length - 1]);
    	res.send(JSON.stringify({ players: players, tournaments: tournaments, lastUpdated: lastUpdated }));
    });
});

var nextScheduledRequestTime = 0;
function throttledRequest(url, callback) {
	var currentTime = new Date().getTime();
	nextScheduledRequestTime += 10000;
	nextScheduledRequestTime = Math.max(currentTime, nextScheduledRequestTime);
	var timeTilNextRequest = nextScheduledRequestTime - currentTime;
	console.log('requesting url', url, 'in', timeTilNextRequest, 'ms');
	setTimeout(function() {
		request(url, function (err, response, body) {
			callback(err, response, body);
		});
	}, timeTilNextRequest);
}

function loadPlayers(page) {
    var url = 'http://www.bluff.com' + page;
	throttledRequest(url, function (err, response, body) {
		console.log('requesting', url);
	    if (err) {
	      return console.error(err);
	    }
	    var parsedHtml = $.load(body);
	    var results = parsedHtml('.dbtres .res');
	    
	    for (var i = 0; i < results.length; i += 2) {
		    var player = $(results[i]);
		    var playerText = player.text();
		    var playerRankAndName = playerText.match(/(\d+)\...(.*)/);
		    var playerRank = playerRankAndName[1];
		    var playerName = playerRankAndName[2];
		    var score = $(results[i + 1]).text();
		    console.log(playerRank, playerName, score);

		    client.zadd('wsop-poy:2013:points', score, playerName);
	    }
	    
	    var nextLink = parsedHtml('a.next-arrow').attr('href');
	    console.log(nextLink);
	    if (nextLink) {
	    	loadPlayers(nextLink);
	    } else {
	      console.log('finished loading players');
	      client.set('wsop-poy:2013:points:last-updated', new Date().getTime());
	    }
	});
}

function getPoolPlayerResults(poolPlayer) {
	var resultsUrl = poolPlayers[poolPlayer].url;
	if (!resultsUrl) {
		return;
	}
	console.log('getPoolPlayerResults', poolPlayer, resultsUrl);
	throttledRequest(resultsUrl, function (err, response, body) {
        if (err) {
          return console.error(err);
        }
        var parsedHtml = $.load(body);
        var resultRows = parsedHtml('.ptres tr');
        
        poolPlayers[poolPlayer].points = 0;
        for (var i = 0; i < resultRows.length; i++) {
            var resultRow = $(resultRows[i]);
            var seriesLink = resultRow.find('td a');
            if (seriesLink.length === 0) {
                continue;
            }
            if (seriesLink.first().text() === '2013 44th Annual World Series of Poker') {
                var poyPoints = resultRow.find('td').last().text();
                poolPlayers[poolPlayer].points += Number(poyPoints);
            }
        }
        for (var p in poolPlayers) {
            console.log(p + ': ' + poolPlayers[p].points);
        }
        poolPlayersLastUpdated = new Date();
	});
}

function loadPoolPlayers() {
	for (var poolPlayer in poolPlayers) {
		getPoolPlayerResults(poolPlayer);
	}
}

function getTournamentName(tournamentResultsPath) {
	return tournamentResultsPath.match(/\/tournaments\/event\/(.*)\.htm.*/)[1];
}

var loadingTournaments = [];
var completeTournaments = {};

function loadTournamentResultsForPoolPlayers(tournamentResultsPath) {
	var tournamentName = getTournamentName(tournamentResultsPath);
	throttledRequest("http://www.bluff.com" + tournamentResultsPath, function (err, response, body) {
		console.log('loadTournamentResultsForPoolPlayers', tournamentResultsPath, tournamentName);
        if (err) {
          return console.error(err);
        }
        var parsedHtml = $.load(body);
        var resultRows = parsedHtml('.tresults tr');

        if (/\.html$/.test(tournamentResultsPath)) {
			var resultsComplete = true;
        	if (parsedHtml('.tresults td:contains("To Be Determined")').length > 0) {
        		resultsComplete = false;
        	}
	        console.log(tournamentName, 'resultsComplete', resultsComplete);
	        completeTournaments[tournamentName] = resultsComplete;
        }
            
        for (var i = 0; i < resultRows.length; i++) {
            var resultRow = $(resultRows[i]);
            var playerLink = resultRow.find('td a');
            if (playerLink.length === 0) {
                continue;
            }
            var poyPointsText = resultRow.find('td').eq(1).text();
            var poyPoints = poyPointsText.match(/([\d\.]+) pts/)[1];
            if (poyPoints === '0.00') {
            	console.log(tournamentName, 'found 0.00 WSOP POY points - resultsComplete false');
            	completeTournaments[tournamentName] = false;
            	continue;
            }

            var playerName = playerLink.first().text();
            if (poolPlayers[playerName]) {
                console.log(playerName, poyPoints);
                client.hset('player:' + playerName + ':tournament-points', tournamentName, poyPoints);
            }
        }

        var nextLink = parsedHtml('a.next-arrow').attr('href');
	    if (nextLink) {
			loadTournamentResultsForPoolPlayers(nextLink);
	    } else {
			console.log('finished loading tournament', tournamentName);

			if (completeTournaments[tournamentName]) {
				console.log('tournament complete', tournamentName);
				client.sadd('loaded-tournaments', tournamentName);
			} else {
				console.log('tournament incomplete', tournamentName);
			}

			loadingTournaments = _.without(loadingTournaments, tournamentName);
			console.log('still loadingTournaments: ', loadingTournaments);
			if (loadingTournaments.length === 0) {
				var lastUpdated = new Date();
				console.log('no loadingTournaments left, setting last updated: ', lastUpdated);
				client.set('tournament-results:last-updated', lastUpdated.getTime());
			}
	    }
	});
}

function loadAllTournamentResultsForPoolPlayers() {
	throttledRequest("http://www.bluff.com/tournaments/2013-44th-annual-world-series-of-poker-2524.htm", function (err, response, body) {
        if (err) {
          return console.error(err);
        }
        client.smembers('loaded-tournaments', function(err, loadedTournaments) {
        	console.log('loaded-tournaments', loadedTournaments);
	        var parsedHtml = $.load(body);
	        var resultLinks = parsedHtml('.tevents td a');
	        
	        for (var i = 0; i < resultLinks.length; i++) {
	        	var tournamentResultsPath = $(resultLinks[i]).attr('href');
	        	var tournamentName = getTournamentName(tournamentResultsPath);
	        	if (_.contains(loadedTournaments, tournamentName) || (/-event-(1|26|51)-/).test(tournamentResultsPath)) {
	        		console.log('already loaded', tournamentName);
	        		continue;
	        	}
	        	loadingTournaments.push(tournamentName);
	        	console.log('scheduling loading of', tournamentName);
	    		loadTournamentResultsForPoolPlayers(tournamentResultsPath);
	    	}
        });
	});
}

var sixHoursInMilliseconds = 1000 * 60 * 60 * 6;

function loadEverySixHours(lastUpdatedKey, loadFunction) {
	client.get(lastUpdatedKey, function(err, lastUpdated) {
		lastUpdated = lastUpdated || 0;
		var currentDate = new Date();
		var lastUpdatedDate = new Date(Number(lastUpdated));
		console.log(lastUpdatedKey, lastUpdatedDate);
		var timeDiff = currentDate.getTime() - lastUpdatedDate.getTime();
		var firstLoadWait = Math.max(sixHoursInMilliseconds - timeDiff, 0);
		console.log(lastUpdatedKey, 'next load scheduled in', firstLoadWait, 'ms');
		setTimeout(function() {
			loadFunction();
			setInterval(function() {
				loadFunction();
			}, sixHoursInMilliseconds);
		}, firstLoadWait);
	});
}

loadEverySixHours('wsop-poy:2013:points:last-updated', function() { loadPlayers('/wsop-poy/'); });
loadEverySixHours('tournament-results:last-updated', loadAllTournamentResultsForPoolPlayers);

server.listen(PORT);
console.log('Listening on port ' + PORT);
