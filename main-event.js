var PORT = process.env.PORT || 8080;

var $ = require('cheerio');
var request = require('request');
var _ = require('underscore');

var poolPlayers = {
    "Daniel Negreanu": { url: "http://www.bluff.com/players/daniel-negreanu/tournament-results/267/" },
	"Jason Mercier": { url: "http://www.bluff.com/players/jason-mercier/tournament-results/42898/" },
	"Bertrand Grospellier": { url: "http://www.bluff.com/players/bertrand-grospellier/tournament-results/32116/" },
	"Eugene Katchalov": { url: "http://www.bluff.com/players/eugene-katchalov/tournament-results/1343/" },
	"John Juanda": { url: "http://www.bluff.com/players/john-juanda/tournament-results/261/" },
	"Jonathan Karamalikis": { url: "http://www.bluff.com/players/jonathan-karamalikis/tournament-results/39764/" },
	"Ana Marquez": { url: "http://www.bluff.com/players/ana-marquez/tournament-results/88879/" },
	"Shawn Buchanan": { url: "http://www.bluff.com/players/shawn-buchanan/tournament-results/1393/" },
	"Matt Glantz": { url: "http://www.bluff.com/players/matt-glantz/tournament-results/616/" },
	"John Monnette": { url: "http://www.bluff.com/players/john-monnette/tournament-results/4028/" },
	"Phil Ivey": { url: "http://www.bluff.com/players/phil-ivey/tournament-results/273/" },
	"Phil Hellmuth": { url: "http://www.bluff.com/players/phil-hellmuth/tournament-results/271/" },
	"Marvin Rettenmaier": { url: "http://www.bluff.com/players/marvin-rettenmaier/tournament-results/82068/" },
	"Paul Volpe": { url: "http://www.bluff.com/players/paul-volpe/tournament-results/53967/" },
	"Maria Ho": { url: "http://www.bluff.com/players/maria-ho/tournament-results/4368/" },
	"Dan Kelly": { url: "http://www.bluff.com/players/daniel-kelly/tournament-results/35756/" },
	"Justin Bonomo": { url: "http://www.bluff.com/players/justin-bonomo/tournament-results/1389/" },
	"Jeff Lisandro": { url: "http://www.bluff.com/players/jeffrey-lisandro/tournament-results/745/" },
	"Toby Lewis": { url: "http://www.bluff.com/players/toby-lewis/tournament-results/65252/" },
	"Jarred Solomon": { url: "http://www.bluff.com/players/jarred-solomon/tournament-results/52009/" },
	"Aaron Lim": { url: "http://www.bluff.com/players/aaron-lim/tournament-results/120180/" },
	"Joe Hachem": { url: "http://www.bluff.com/players/joe-hachem/tournament-results/3426/" },
	"Shaun Deeb": { url: "http://www.bluff.com/players/shaun-deeb/tournament-results/26176/" },
	"Alexander Kostritsyn": { url: "http://www.bluff.com/players/alexander-kostritsyn/tournament-results/40410/" },
	"Thayer Rasmussen": { url: "http://www.bluff.com/players/thayer-rasmussen/tournament-results/19953/" },
	"Xuan Liu": { url: "http://www.bluff.com/players/xuan-liu/tournament-results/80564/" },
	"David \"Bakes\" Baker": { url: "http://www.bluff.com/players/david-baker/tournament-results/1915/" },
	"Johnny Lodden": { url: "http://www.bluff.com/players/johnny-lodden/tournament-results/29360/" },
	"Sorel Mizzi": { url: "http://www.bluff.com/players/sorel-mizzi/tournament-results/29488/" },
	"Steve O'Dwyer": { url: "http://www.bluff.com/players/steve-o'dwyer/tournament-results/26145/" },
	"Victor Ramdin": { url: "http://www.bluff.com/players/victor-ramdin/tournament-results/720/" },
	"Martin Finger": { url: "http://www.bluff.com/players/martin-finger/tournament-results/117122/" },
	"Stephen Chidwick": { url: "http://www.bluff.com/players/stephen-chidwick/tournament-results/42335/" },
	"Kitty Kuo": { url: "http://www.bluff.com/players/hui-chen-kuo/tournament-results/68620/" },
	"Gregory Ronaldson": { url: "http://www.bluff.com/players/gregory-ronaldson/tournament-results/102685/" },
	"Kosta Mamaliadis": {},
	"Takashi Ogura": { url: "http://www.bluff.com/players/Takashi-Ogura/tournament-results/62880/"}
};

var nextScheduledRequestTime = 0;
function throttledRequest(url, callback) {
	var currentTime = new Date().getTime();
	nextScheduledRequestTime += 10000;
	nextScheduledRequestTime = Math.max(currentTime, nextScheduledRequestTime);
	var timeTilNextRequest = nextScheduledRequestTime - currentTime;
	console.log('requesting url', url, 'in', timeTilNextRequest, 'ms');
	setTimeout(function() {
		request(url, function (err, response, body) {
			callback(err, response, body);
		});
	}, timeTilNextRequest);
}

function getMainEventChipCounts(day) {
    var url = 'http://www.pokernews.com/live-reporting/2013-world-series-of-poker/event-62-10-000-no-limit-hold-em-main-event/day' + day + '/chips.htm';
	throttledRequest(url, function (err, response, body) {
		console.log('requesting', url);
	    if (err) {
	      return console.error(err);
	    }
	    var parsedHtml = $.load(body);
	    var chipCounts = parsedHtml('table.chipCounts tr');
	    
	    for (var i = 0; i < chipCounts.length; i++) {
		    var playerData = $(chipCounts[i]).find('td');
		    var playerName = playerData.eq(0).text();
		    var playerChipCount = playerData.eq(4).text();
		    if (poolPlayers[playerName]) {
		    	console.log(playerName, playerChipCount);
		    }
	    }
	});
}

//var days = ['1a', '1b', '1c' ];
var days = ['4' ];
for (var i in days) {
	getMainEventChipCounts(days[i]);
}
