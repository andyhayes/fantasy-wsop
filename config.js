var config = {
};

config.NODE_ENV = process.env.NODE_ENV || 'development';
config.DEVELOPMENT = config.NODE_ENV === 'development';
config.REDIS_HOST = process.env.REDIS_HOST || 'localhost'; 
config.REDIS_PORT = process.env.REDIS_PORT || '6379';
config.REDIS_PASS = process.env.REDIS_PASS || 'foobared'; 

exports.config = config;
